CREATE TABLE magasin(
codesociete VARCHAR(3) PRIMARY KEY,
ville VARCHAR(27),
libelledepartement VARCHAR(2),
libelleregioncommerciale VARCHAR(14)
);

CREATE TABLE article(
codearticle VARCHAR(6) PRIMARY KEY,
codeunivers VARCHAR(6),
codefamille VARCHAR(6),
codesousfamille VARCHAR(6)
);

CREATE TABLE client(
	idclient VARCHAR(7) PRIMARY KEY,
	civilte VARCHAR (8),
	datenaissance DATE,
	magasin VARCHAR (3) REFERENCES magasin(codesociete), -- foreign key
	datedebutadhesion DATE,
	datereadhesion DATE,
	datefinadhesion DATE,
	VIP boolean,
	codeinsee VARCHAR (6),
	pays VARCHAR (2)
);

CREATE TABLE entetes_ticket(
	idticket bigserial PRIMARY KEY,  -- faster than character
	tic_date TIMESTAMP, -- no tz timezone
	mag_code VARCHAR (3) REFERENCES magasin(codesociete),  -- foreign key
	idclient VARCHAR(7) REFERENCES client(idclient),  -- foreign key 
	tic_totalttc NUMERIC(8, 2)  -- 8 digits precision, 2 decimal places
);

CREATE TABLE lignes_ticket(
	idticket bigserial,  -- non-unique
	numligneticket SMALLINT, --  3 digits width only
	idarticle VARCHAR (6) REFERENCES article(codearticle),  -- foreign key
	quantite NUMERIC(7, 3), -- 3 decimal places in data
	montantremise NUMERIC(9, 3),  -- 3 decimal places
	total NUMERIC(8, 2),
	margesortie NUMERIC(8, 2));

-- Example load test code used for CLIENT table in postgresql
-- \copy MAGASIN FROM '/full_path_to_file/MAGASIN_cleaned.csv' DELIMITER ',' CSV HEADER


