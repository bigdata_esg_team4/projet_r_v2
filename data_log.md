## Log de transformations des données  

### client  
- idclient (character) : non
- civilite (character) : merge des civilités mr, monsieur etc
- datenaissance (Date) : conversion de caractères datetime à date
- magasin (character) : non
- datedebutadhesion (Date) : conversion de caractères datetime à date
- datereadhesion (Date) : conversion de caractères datetime à date
- datefinadhesion (Date) : conversion de caractères datetime à date
- vip (integer) : non  
- codeinsee (character) : remplacement de '' par NA; suppression de caractères invisible (idclient == 1241939)
- pays (character) : non
- question 1.2 : suppression des lignes avec date de fin d'adhésion aberrantes (> "2020-03-30")
- question 1.3 : suppression des lignes avec datenaissance vides
- question 1.3 : datenaissance aberrantes (< 1921-09-07, > 2014-06-12)
- question 1.3 : datanaissance suppresion des lignes avec 'age' de < 18 ans

### entetes_ticket  
- idticket (integer64) : non. *Integer64 est conseillé pour des index de tables volumineaux*)
- tic_date (POSIXct" "POSIXt"): conversion de caractères à datetime
- mag_code (character) : non
- idclient (character) : non
- tic_totalttc (numeric) : non
  
### lignes_ticket  
- idticket (integer64) : non
- numligneticket (integer) : non
- idarticle (character) : non
- quantite (numeric) : non
- montantremise (numeric) : non
- total (numeric) : none
- margesortie (numeric)  : non

### article  
- codearticle (character) : ajoute de clé manquant (codearticle == 395460) avec valeurs NA (voir clean.R pour plus de détails)
- codeunivers (character) : non
- codefamille (character) : non
- codesousfamille (character) : non

### magasin  
- codesociete (character) : non
- ville (character) : non
- libelledepartement (character) : non
- libelleregioncommerciale (character) : non





